<div class="navbar navbar-expand-md navbar-dark mb-3" style="position:fixed;width:100%; z-index:1">
	<div class="navbar-brand ">
		<center>
			<a href="{{URL::to('/')}}" class="d-inline-block">
				<h1 class="mb-0 font-weight-black" style="color:white">Apotech.id</h1><
			</a>
		/center>
	</div>

	<div class="d-md-none">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
			<i class="icon-tree5"></i>
		</button>
		<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
			<i class="icon-paragraph-justify3"></i>
		</button>
	</div>

	<div class="collapse navbar-collapse" id="navbar-mobile">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
					<i class="icon-paragraph-justify3"></i>
				</a>
			</li>
		</ul>

		
		<span class="badge bg-success-400 ml-md-auto mr-md-2">Active</span>
		<ul class="navbar-nav">
			<li class="nav-item dropdown dropdown-user">
				
				<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
					<span><h5 class="mb-0 font-weight-bold">{{ Auth::user()->name }}</h5></span>
				</a>
				
				<div class="dropdown-menu dropdown-menu-right">
					<a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" class="dropdown-item">
                        <i class="icon-switch2"></i> Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
				</div>
			</li>
		</ul>
	</div>
</div>
