<!DOCTYPE html>
<html lang="en">
<head>
	<style>
		::-webkit-scrollbar {
			width: 0px; 
			background: transparent;
		}
	</style>
	@include('layouts.includes.head')
	@yield('header')

	@include('layouts.includes.script')
</head>

<body>

	<!-- Main navbar -->
	@include('layouts.includes.navbar')
	<!-- /main navbar -->

	<!-- Page header -->
	{{-- @include('layouts.includes.breadcrumb') --}}
	<!-- /page header -->
		
	<!-- Page content -->
	<div class="page-content pt-0">

		<!-- Main sidebar -->
		@include('layouts.includes.menu')
		<!-- /main sidebar -->


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content" style="margin-left: 20%;margin-top : 6%; ">
				@yield('content')
			</div>
			<!-- /content area -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->


	<!-- Footer -->
	@include('layouts.includes.footer')
	<!-- /footer -->
		
</body>
</html>
